/**
 * Vytvoreno: rijen 2017
 * Autor: Jan Herec
 * Popis: Aplikace pro Vizualizaci Juliovych mnozin, vytvorena v ramci predmetu SFC na FIT VUT v Brně
 * Kodovani: UTF-8
 */

package com.sfcproject.juliaset;

import javax.imageio.*;
import javax.imageio.metadata.IIOInvalidTreeException;
import javax.imageio.stream.ImageInputStream;
import javax.imageio.stream.ImageOutputStream;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.io.*;
import java.util.Iterator;

/**
 * Trida reprezentujii vizualizaci Juliovy mnoziny
 * Zajistuje vizualizaci Juliovy mnoziny, podle zadanych parametru a ulozeni/nacteni vizualizace na/z disk(u)
 */
public class JuliasetVisualisation extends JPanel {
    private int PANEL_WIDTH = 680;                                  // sirka panelu ve kterem se vizualizuje Juliova množina
    private int PANEL_HEIGHT = 630;                                 // vyska panelu ve kterem se vizualizuje Juliova množina
    public int IMAGE_WIDTH = 600;                                   // sirka obrazku predstavujici vizualizaci Juliovy mnoziny
    public int IMAGE_HEIGHT = 600;                                  // vyska obrazku predstavujici vizualizaci Juliovy mnoziny
    BufferedImage image = null;                                     // obrazek predstavujici vizualizaci Juliovy mnoziny
    private double complexPlaneSelectionWidth = 3.5;                // sirka vybrane oblasti komplexni roviny, ve které se bude vizualizovat juliova mnozina
    private double complexPlaneSelectionHeight = 3.5;               // vyska vybrane oblasti komplexni roviny, ve které se bude vizualizovat juliova mnozina
    private double complexPlaneSelectionCenterRealPart = 0.0;       // pozice stredu na realne ose vybrane oblasti komplexni roviny, ve které se bude vizualizovat juliova mnozina
    private double complexPlaneSelectionCenterImaginaryPart = 0.0;  // pozice stredu na imaginarni ose vybrane oblasti komplexni roviny, ve které se bude vizualizovat juliova mnozina
    private ComplexNumber c;                                        // parametr c, ktery specifikuje konkretni Juliovu mnozinu
    private Color juliaColor;                                       // barva bodu patrici do Juliovy mnoziny
    private Color startColor;                                       // bava bodu u nehoz se zjisti v posledni iteraci ze nepatri do mnoziny
    private Color endColor;                                         // barva bodu u nehoz se zjisti v 1. iteraci ze nepatri do mnoziny
    private int maxIterations;                                      // max pocet iteraci pri zjistovani jestli dany bod patri nebo nepatric do juliovy mnoziny
    private double animationStepRealC;                              // hodnota ktera se pricte k realne casti parametru c v jednom animacnim kroku
    private double animationStepImaginaryC;                         // hodnota ktera se pricte k imaginarni casti parametru c v jednom animacnim kroku

    public JuliasetVisualisation() {
        setPreferredSize(new Dimension(PANEL_WIDTH, PANEL_HEIGHT));
    }

    /**
     * resetuje pohled na puvodni
     */
    public void resetView() {
        complexPlaneSelectionWidth = 3.5;
        complexPlaneSelectionHeight = 3.5;
        complexPlaneSelectionCenterRealPart = 0.0;
        complexPlaneSelectionCenterImaginaryPart = 0.0;
    }

    /**
     * getter
     */
    public BufferedImage getImage() {
        return image;
    }
    /**
     * setter
     */
    public void setC(double realPart, double imaginaryPart) {
        this.c = new ComplexNumber(realPart, imaginaryPart);
    }
    /**
     * getter
     */
    public ComplexNumber getC() {
        return this.c;
    }
    /**
     * setter
     */
    public void setAnimationStepRealC(double animationStepRealC) {
        this.animationStepRealC = animationStepRealC;
    }
    /**
     * getter
     */
    public double getAnimationStepRealC() {
        return this.animationStepRealC;
    }
    /**
     * setter
     */
    public void setAnimationStepImaginaryC(double animationStepImaginaryC) {
        this.animationStepImaginaryC = animationStepImaginaryC;
    }
    /**
     * getter
     */
    public double getAnimationStepImaginaryC() {
        return this.animationStepImaginaryC;
    }
    /**
     * setter
     */
    public void setJuliaColor(Color juliaColor) {
        this.juliaColor = juliaColor;
    }
    /**
     * getter
     */
    public Color getJuliaColor() {
        return this.juliaColor;
    }
    /**
     * setter
     */
    public void setStartColor(Color startColor) {
        this.startColor = startColor;
    }
    /**
     * getter
     */
    public Color getStartColor() {
        return this.startColor;
    }
    /**
     * setter
     */
    public void setEndColor(Color endColor) {
        this.endColor = endColor;
    }
    /**
     * getter
     */
    public Color getEndColor() { return this.endColor; }
    /**
     * setter
     */
    public void setMaxIterations(int maxIterations) {
        this.maxIterations = maxIterations;
    }
    /**
     * getter
     */
    public int getMaxIterations() { return this.maxIterations; }

    /**
     * Zmena velikosti vybrane oblasti komplexni roviny na zaklade toceni koleckem mysi = tedy zmena zoomu
     * @param magnitude mira zvetseni - odpovida toceni koleckem mysi
     */
    public void changeZoom(int magnitude) {
        complexPlaneSelectionWidth += (complexPlaneSelectionWidth / 100.0 * magnitude) * 1.8;
        complexPlaneSelectionHeight += (complexPlaneSelectionHeight / 100.0 * magnitude) * 1.8;
    }
    /**
     * Zmena stredu vybrane oblasti komplexni roviny na zaklade tahnuti mysi
     * @param dx posun k novemu stredu ve smeru realne osy
     * @param dy posun k novemu stredu ve smeru imaginarni osy
     */
    public void changeCenter(int dx, int dy) {
        complexPlaneSelectionCenterRealPart += 0.3 * ((-1) * dx * complexPlaneSelectionWidth / IMAGE_WIDTH);
        complexPlaneSelectionCenterImaginaryPart += 0.3 * (dy * complexPlaneSelectionHeight / IMAGE_HEIGHT);
    }

    /**
     * Vykresleni Juliovy mnoziny
     */
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        // pripravime si obrazek ktery budeme vykreslovat
        createRasterImage(IMAGE_WIDTH, IMAGE_HEIGHT);

        g.drawImage(image, 80, 0, IMAGE_WIDTH, IMAGE_HEIGHT, null);
        g.setFont(new Font("Courier", Font.PLAIN, 12));
        g.drawString(String.format("%.3e",complexPlaneSelectionCenterImaginaryPart + complexPlaneSelectionHeight / 2) + "i", 0, 12);
        g.drawString(String.format("%.3e",complexPlaneSelectionCenterImaginaryPart - complexPlaneSelectionHeight / 2)+ "i", 0, IMAGE_HEIGHT - 5);
        g.drawString(String.format("%.3e",complexPlaneSelectionCenterRealPart - complexPlaneSelectionWidth / 2), 80, IMAGE_HEIGHT + 15);
        g.drawString(String.format("%.3e",complexPlaneSelectionCenterRealPart + complexPlaneSelectionWidth / 2), IMAGE_WIDTH + 15, IMAGE_HEIGHT + 15);
    }

    /**
     * Funkce vytvoří obrázek, který se bude vykreslovat nebo ukládat, tento obtázek se uloží so interního atributu image
     * @param imageWidth sirka vykreslovaneho/ukladaneho obrazku
     * @param imageHeight vyska vykreslovaneho/ukladaneho obrazku
     */
    public void createRasterImage(int imageWidth, int imageHeight) {
        // přepočítáme oblast komplexní roviny který zobrazíme
        double originNewWidthRatio = (double) imageWidth / (double) IMAGE_WIDTH;
        double originNewHeightRatio = (double) imageHeight / (double) IMAGE_HEIGHT;
        double newComplexPlaneSelectionWidth = complexPlaneSelectionWidth * originNewWidthRatio;
        double newComplexPlaneSelectionHeight = complexPlaneSelectionHeight * originNewHeightRatio;

        double realPartSpacing = newComplexPlaneSelectionWidth / imageWidth; // vzdalenost mezi sousednimi body v komplexni rovine na realne slozce
        double imaginaryPartSpacing = newComplexPlaneSelectionHeight / imageHeight; // vzdalenost mezi sousednimi body v komplexni rovine na imaginarni slozce

        double firstRealPart = complexPlaneSelectionCenterRealPart - newComplexPlaneSelectionWidth / 2; // realna slozka leve horni pozice v aktualne zobrazene komplexni rovine
        double firstImaginaryPart = complexPlaneSelectionCenterImaginaryPart + newComplexPlaneSelectionHeight / 2; // imaginarni slozka leve horni pozice v aktualne zobrazene komplexni rovine

        double currentImaginaryPart; // realna slozka soucasna pozice v aktualne zobrazene komplexni rovine
        double currentRealPart;  // imaginarni slozka soucasna pozice v aktualne zobrazene komplexni rovine
        int row; // aktualni radek obrazku
        int col; // aktualni sloupec obrazku

        // spocitame barevny posun na iteraci resp pocet iteraci na barevny posun (pokud je rzdil dane baevne slozky menis jak max. pocet iteraci)
        double redDeltaAfterNumOfIterations = 0;
        double greenDeltaAfterNumOfIterations = 0;
        double blueDeltaAfterNumOfIterations = 0;
        double redDelta = endColor.getRed() - startColor.getRed();
        double greenDelta = endColor.getGreen() - startColor.getGreen();
        double blueDelta = endColor.getBlue() - startColor.getBlue();

        // spocitame pocet iteraci na barevny posun v cervenem spektru (pokud je rozdil dane baevne slozky menis jak max. pocet iteraci)
        if (redDelta != 0 && Math.abs(redDelta) < maxIterations) {
            redDeltaAfterNumOfIterations = maxIterations / (double) Math.abs(redDelta);
            redDelta = redDelta / Math.abs(redDelta); // zachovame znamekovy jednotovy posun po danem poctu iteraci
        }
        // jinak spoticame veliksot barevneho posunu v cervenem spektru na iteraci
        else if (redDelta != 0){
            redDelta = redDelta / maxIterations;
        }

        // spocitame pocet iteraci na barevny posun v zelenem spektru (pokud je rozdil dane baevne slozky menis jak max. pocet iteraci)
        if (greenDelta != 0 && Math.abs(greenDelta) < maxIterations) {
            greenDeltaAfterNumOfIterations = maxIterations / Math.abs(greenDelta);
            greenDelta = greenDelta / Math.abs(greenDelta); // zachovame znamekovy jednotovy posun po danem poctu iteraci
        }
        // jinak spoticame veliksot barevneho posunu v zelenem spektru na iteraci
        else if (greenDelta != 0){
            greenDelta = greenDelta / maxIterations;
        }

        // spocitame pocet iteraci na barevny posun v modrem spektru (pokud je rozdil dane baevne slozky menis jak max. pocet iteraci)
        if (blueDelta != 0 && Math.abs(blueDelta) < maxIterations) {
            blueDeltaAfterNumOfIterations = maxIterations / (double) Math.abs(blueDelta);
            blueDelta = blueDelta / Math.abs(blueDelta); // zachovame znamekovy jednotovy posun po danem poctu iteraci
        }
        // jinak spoticame veliksot barevneho posunu v modrem spektru na iteraci
        else if (blueDelta != 0){
            blueDelta = blueDelta / maxIterations;
        }
        // obrazek predsatvujici vizualizaci
        image = new BufferedImage(imageWidth, imageHeight, BufferedImage.TYPE_INT_RGB);

        WritableRaster raster = image.getRaster();

        // po radcich a sloupcich nastavujee pixely obrazku podle barev odpovidajicich pocatecnch bodu komplexni roviny pro ktere zjistujeme jestli patri do Juliovy mnoziny
        for (row = 0, currentImaginaryPart = firstImaginaryPart; row < imageHeight; row++, currentImaginaryPart -= imaginaryPartSpacing) {
            for (col = 0, currentRealPart = firstRealPart; col < imageWidth; col++, currentRealPart += realPartSpacing) {

                // pocatecni bod pro ktery budeme zjistovat jestli pari do Juliovy mnoziny
                ComplexNumber z = new ComplexNumber(currentRealPart, currentImaginaryPart);

                boolean belongToJuliaSet = true; // priznak jestli pocatecni bod patri do mnoziny
                int[] rgba = {0, 0, 0, 255}; // barva bodu
                // postupne v iteracich zjistujeme pro pocatecni bod jestli patri do Juliovy mnoziny
                for (int i = 0; i < maxIterations; i++) {
                    z = z.square(); // umocnime pocatecni bod
                    z.add(c);       // k pocatecnimu obdu pripocteme parametr c
                    // pokud poslopnost diverguje, bod nelezi v Juliove mnozine, vypocitame jeho barvu
                    if (z.abs() > 2.0) {

                        // pro cervenou barevnou slozku urcime jeji hodnotu podle aktualni iterace i
                        if (redDelta != 0) {
                            if (redDeltaAfterNumOfIterations == 0) {
                                rgba[0] = Utils.getMinMax(255, 0, startColor.getRed() + (int) (redDelta * (maxIterations - (i + 1))));
                            }
                            else {
                                rgba[0] = Utils.getMinMax(255, 0, startColor.getRed() + (int) (redDelta * (Math.round(((maxIterations - (i + 1)) / redDeltaAfterNumOfIterations)))));
                            }
                        }
                        // pro zelenou barevnou slozku urcime jeji hodnotu podle aktualni iterace i
                        if (greenDelta != 0) {
                            if (greenDeltaAfterNumOfIterations == 0) {
                                rgba[1] = Utils.getMinMax(255, 0, startColor.getGreen() + (int) (greenDelta * (maxIterations - (i + 1))));
                            }
                            else {
                                rgba[1] = Utils.getMinMax(255, 0, startColor.getGreen() + (int) (greenDelta * (Math.round(((maxIterations - (i + 1)) / greenDeltaAfterNumOfIterations)))));
                            }
                        }
                        // pro modrou barevnou slozku urcime jeji hodnotu podle aktualni iterace i
                        if (blueDelta != 0) {
                            if (blueDeltaAfterNumOfIterations == 0) {
                                rgba[2] = Utils.getMinMax(255, 0, startColor.getBlue() + (int) (blueDelta * (maxIterations - (i + 1))));
                            }
                            else {
                                rgba[2] = Utils.getMinMax(255, 0, startColor.getBlue() + (int) (blueDelta * (Math.round(((maxIterations - (i + 1)) / blueDeltaAfterNumOfIterations)))));
                            }
                        }
                        belongToJuliaSet = false;
                        break;
                    }
                }

                // pokud pocatecni bod patri do mnoziny, obarvime jej tak
                if (belongToJuliaSet == true) {
                    rgba[0] = juliaColor.getRed();
                    rgba[1] = juliaColor.getGreen();
                    rgba[2] = juliaColor.getBlue();
                }

                // nasavime dany pixel podle barvy bodu
                raster.setPixel(col, row, rgba);
            }
        }
    }

    /**
     * Funkce ulozi vizualizaci jako obrazek s metadaty popisujici aktualni konfiguraci zobrazeni
     * @param tmpFile docasny soubor s viualizaci bez metadat
     * @param fileToSave soubor kam se bue ukladat viualizace s metadaty
     */
    public void saveVisualizationToFile(File tmpFile, File fileToSave) {
        try {
            ImageInputStream input = ImageIO.createImageInputStream(tmpFile);
            ImageOutputStream output = ImageIO.createImageOutputStream(fileToSave);

            Iterator<ImageReader> readers = ImageIO.getImageReaders(input);
            ImageReader reader = readers.next();

            reader.setInput(input);
            IIOImage image = reader.readAll(0, null);

            // ulozime do metadat aktalni parametry vizualizace
            Utils.addTextEntryToImageMetadata(image.getMetadata(), "realPartOfC", String.valueOf(this.c.getRealPart()));
            Utils.addTextEntryToImageMetadata(image.getMetadata(), "imaginaryPartOfC", String.valueOf(this.c.getImaginaryPart()));
            Utils.addTextEntryToImageMetadata(image.getMetadata(), "animationStepRealC", String.valueOf(this.animationStepRealC));
            Utils.addTextEntryToImageMetadata(image.getMetadata(), "animationStepImaginaryC", String.valueOf(this.animationStepImaginaryC));

            Utils.addTextEntryToImageMetadata(image.getMetadata(), "juliaColor", Integer.toString(juliaColor.getRGB()));
            Utils.addTextEntryToImageMetadata(image.getMetadata(), "startColor", Integer.toString(startColor.getRGB()));
            Utils.addTextEntryToImageMetadata(image.getMetadata(), "endColor", Integer.toString(endColor.getRGB()));
            Utils.addTextEntryToImageMetadata(image.getMetadata(), "maxIterations", String.valueOf(this.maxIterations));

            Utils.addTextEntryToImageMetadata(image.getMetadata(), "complexPlaneSelectionWidth", String.valueOf(this.complexPlaneSelectionWidth));
            Utils.addTextEntryToImageMetadata(image.getMetadata(), "complexPlaneSelectionHeight", String.valueOf(this.complexPlaneSelectionHeight));
            Utils.addTextEntryToImageMetadata(image.getMetadata(), "complexPlaneSelectionCenterRealPart", String.valueOf(this.complexPlaneSelectionCenterRealPart));
            Utils.addTextEntryToImageMetadata(image.getMetadata(), "complexPlaneSelectionCenterImaginaryPart", String.valueOf(this.complexPlaneSelectionCenterImaginaryPart));


            ImageWriter writer = ImageIO.getImageWriter(reader);
            writer.setOutput(output);
            writer.write(image);
            writer.dispose();
            reader.dispose();
            input.close();
            output.close();
        }
        catch (IIOInvalidTreeException e) {
            JOptionPane.showMessageDialog(this, "Error during saving to file", "IIOInvalidTreeException", JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
        } catch (IOException e) {
            JOptionPane.showMessageDialog(this, "Error during saving to file", "IOException", JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
        }
    }

    /**
     * Funkce otevre vizualizaci jako obrazek s metadaty popisujici aktualni konfiguraci zobrazeni
     * @param inputFile soubor PNG ktery predstavuje vizualiaci a podle jeho metadat obsahujici paramety vizualizace tuto vizualiaci obnovime
     */
    public void openVisualizationFromFile(File inputFile) {
        try {
            ImageInputStream input = ImageIO.createImageInputStream(inputFile);
            Iterator<ImageReader> readers = ImageIO.getImageReaders(input);
            ImageReader reader = readers.next();

            reader.setInput(input);

            // obnovime z metadat obrazku parametry vizualiazce
            try {
                this.c.setRealPart(Double.parseDouble(Utils.getTextEntryFromImageMetadata(reader.getImageMetadata(0), "realPartOfC")));
                this.c.setImaginaryPart(Double.parseDouble(Utils.getTextEntryFromImageMetadata(reader.getImageMetadata(0), "imaginaryPartOfC")));

                this.startColor = new Color(Integer.parseInt(Utils.getTextEntryFromImageMetadata(reader.getImageMetadata(0), "startColor")), true);
                this.endColor = new Color(Integer.parseInt(Utils.getTextEntryFromImageMetadata(reader.getImageMetadata(0), "endColor")), true);
                this.maxIterations = Integer.parseInt((Utils.getTextEntryFromImageMetadata(reader.getImageMetadata(0), "maxIterations")));

                this.complexPlaneSelectionWidth = Double.parseDouble((Utils.getTextEntryFromImageMetadata(reader.getImageMetadata(0), "complexPlaneSelectionWidth")));
                this.complexPlaneSelectionHeight = Double.parseDouble((Utils.getTextEntryFromImageMetadata(reader.getImageMetadata(0), "complexPlaneSelectionHeight")));
                this.complexPlaneSelectionCenterRealPart = Double.parseDouble((Utils.getTextEntryFromImageMetadata(reader.getImageMetadata(0), "complexPlaneSelectionCenterRealPart")));
                this.complexPlaneSelectionCenterImaginaryPart = Double.parseDouble((Utils.getTextEntryFromImageMetadata(reader.getImageMetadata(0), "complexPlaneSelectionCenterImaginaryPart")));


                this.animationStepRealC = Double.parseDouble(Utils.getTextEntryFromImageMetadata(reader.getImageMetadata(0), "animationStepRealC"));
                this.animationStepImaginaryC = Double.parseDouble(Utils.getTextEntryFromImageMetadata(reader.getImageMetadata(0), "animationStepImaginaryC"));
                this.juliaColor = new Color(Integer.parseInt(Utils.getTextEntryFromImageMetadata(reader.getImageMetadata(0), "juliaColor")), true);

            }
            // pokud by chybela metadata tak to zase tak moc nevadi...
            catch(Exception e) {
                System.err.println("Some metadata missing...");
                JOptionPane.showMessageDialog(this, "Some metadata missing, not all visualization parameters were restored.", "Metadata missing", JOptionPane.ERROR_MESSAGE);
            }

            reader.dispose();
            input.close();
        }
        catch (IIOInvalidTreeException e) {
            JOptionPane.showMessageDialog(this, "Error during opening file", "IIOInvalidTreeException", JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
        }
        catch (IOException e) {
            JOptionPane.showMessageDialog(this, "Error during opening file", "IOException", JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
        }
        catch(Exception e) {
            JOptionPane.showMessageDialog(this, "Error during opening file", "Exception", JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
        }
    }
}
