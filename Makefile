all:
	ulimit -v unlimited
	rm -f sfcproject-juliaset.jar
	rm -rf ./*.class
	javac -d . -sourcepath src src/com/sfcproject/juliaset/App.java src/com/sfcproject/juliaset/JuliasetVisualisation.java src/com/sfcproject/juliaset/ComplexNumber.java src/com/sfcproject/juliaset/Utils.java
	jar cvfe sfcproject-juliaset.jar com.sfcproject.juliaset.App com/sfcproject/juliaset/*.class
run:
	java -jar sfcproject-juliaset.jar
