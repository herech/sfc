/**
 * Vytvoreno: rijen 2017
 * Autor: Jan Herec
 * Popis: Aplikace pro Vizualizaci Juliovych mnozin, vytvorena v ramci predmetu SFC na FIT VUT v Brně
 * Kodovani: UTF-8
 */

package com.sfcproject.juliaset;

import org.w3c.dom.NodeList;

import javax.imageio.metadata.IIOInvalidTreeException;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.metadata.IIOMetadataFormatImpl;
import javax.imageio.metadata.IIOMetadataNode;

/**
 * Trida obsahuje uzitecne utilitky, hlavne pro specializovanou praci s metadaty obrazku PNG
 */
public class Utils {

    /**
     * Funkce vrati predanou hodnotu val pokud lezi v danem intervalu danym hodnotami min a max
     * Pokud tato hodnota lezi mimo interval je oriznuta na maximum, reps minimum z intervalu
     * Jedna se o kombinaci Math.max a Math.min
     */
    public static int getMinMax(int max, int min, int val) {
        if (val <= max && val >= min) {
            return val;
        }
        else if (val > max) {
            return max;
        }
        else {
            return min;
        }
    }

    /**
     * Kód převzat z: https://stackoverflow.com/questions/41265608/png-metadata-read-and-write
     * Funkce do metadat obrazku vlozi dany klic a k nemu prislusejici hodnotu
     * @throws IIOInvalidTreeException
     */
    public static void addTextEntryToImageMetadata(final IIOMetadata metadata, final String key, final String value) throws IIOInvalidTreeException {
        IIOMetadataNode textEntry = new IIOMetadataNode("TextEntry");
        textEntry.setAttribute("keyword", key);
        textEntry.setAttribute("value", value);

        IIOMetadataNode text = new IIOMetadataNode("Text");
        text.appendChild(textEntry);

        IIOMetadataNode root = new IIOMetadataNode(IIOMetadataFormatImpl.standardMetadataFormatName);
        root.appendChild(text);

        metadata.mergeTree(IIOMetadataFormatImpl.standardMetadataFormatName, root);
    }

    /**
     * Kód převzat z: https://stackoverflow.com/questions/41265608/png-metadata-read-and-write
     * Funkce z metadat obrazku ziska hodnotu pod danym klicem
     */
    public static String getTextEntryFromImageMetadata(final IIOMetadata metadata, final String key) {
        IIOMetadataNode root = (IIOMetadataNode) metadata.getAsTree(IIOMetadataFormatImpl.standardMetadataFormatName);
        NodeList entries = root.getElementsByTagName("TextEntry");

        for (int i = 0; i < entries.getLength(); i++) {
            IIOMetadataNode node = (IIOMetadataNode) entries.item(i);
            if (node.getAttribute("keyword").equals(key)) {
                return node.getAttribute("value");
            }
        }

        return null;
    }
}
