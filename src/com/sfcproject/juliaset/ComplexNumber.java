/**
 * Vytvoreno: rijen 2017
 * Autor: Jan Herec
 * Popis: Aplikace pro Vizualizaci Juliovych mnozin, vytvorena v ramci predmetu SFC na FIT VUT v Brně
 * Kodovani: UTF-8
 */

package com.sfcproject.juliaset;

import java.lang.Math;

/**
 * Trida reprezentujici komplexni cislo a operace nad nim
 */
public class ComplexNumber {

    private double realPart;      // realna cast komplexniho cisla
    private double imaginaryPart; // imaginarni cast komplexniho cisla

    public ComplexNumber(double realPart, double imaginaryPart) {
        this.realPart = realPart;
        this.imaginaryPart = imaginaryPart;
    }

    public ComplexNumber() {
        this.imaginaryPart = 0.0;
        this.realPart = 0.0;
    }

    /**
     * getter
     */
    public double getRealPart() {
        return this.realPart;
    }

    /**
     * getter
     */
    public double getImaginaryPart() {
        return this.imaginaryPart;
    }

    /**
     * setter
     */
    public void setRealPart(double realPart) {
        this.realPart = realPart;
    }

    /**
     * setter
     */
    public void setImaginaryPart(double imaginaryPart) {
        this.imaginaryPart = imaginaryPart;
    }

    /**
     * Funkce secte aktualni komplexni cislo s jinym komplexnim cislem ktere je predano jako parametr
     * @param complexNumber komplexni cislo s kterym s bude aktualni komplexni cislo scitat
     * @return vysledne kompexni cislo po secteni
     */
    public void add(ComplexNumber complexNumber) {
        this.realPart += complexNumber.realPart;
        this.imaginaryPart += complexNumber.imaginaryPart;
    }

    /**
     * Funkce  vrati ctverec aktualniho komplexniho cisla
     * @return ctverec komplexniho cisla
     */
    public ComplexNumber square() {
        double newRealPart =  this.realPart * this.realPart - this.imaginaryPart * this.imaginaryPart;
        double newImaginaryPart =  this.imaginaryPart * this.realPart + this.realPart * this.imaginaryPart;
        return new ComplexNumber(newRealPart, newImaginaryPart);

    }

    /**
     * Funkce vraci absolutni hodnotu aktualniho kompleniho cisla
     * @return absolutni hodnota komplexniho cisla
     */
    public double abs() {
        return Math.sqrt(this.realPart * this.realPart + this.imaginaryPart * this.imaginaryPart);
    }


    @Override
    public String toString() {
        return "( " + this.realPart + ", " + this.imaginaryPart + " )";
    }

}
