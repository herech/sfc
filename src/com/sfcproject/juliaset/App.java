/**
 * Vytvoreno: rijen 2017
 * Autor: Jan Herec
 * Popis: Aplikace pro Vizualizaci Juliovych mnozin, vytvorena v ramci predmetu SFC na FIT VUT v Brně
 * Kodovani: UTF-8
 */

package com.sfcproject.juliaset;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.plaf.FontUIResource;
import java.awt.*;
import java.awt.event.*;
import java.io.*;

/**
 * Trida reprezentujii hlavni obrazovku aplikace
 * Zajistuje vykresleni GUI a obsluhu udalosti
 */
public class App extends JPanel {

    private JPanel mainJPanel;                               // hlavni panel
    private JMenuBar mainJMenuBar;                           // hlavni menu
    private JMenuItem openJMenuItem;                         // polozka menu open visualization
    private JMenuItem saveJMenuItem;                         // polozka menu save visualization
    private JMenuItem resetViewJMenuItem;                    // polozka menu reset visualization
    private JMenuItem helpJMenuItem;                         // polozka menu help
    private JMenuItem aboutJMenuItem;                        // polozka menu about program
    private JPanel rightJPanel;                              // pravy panel s parametry
    private JButton juliaColorJButton;                       // tlacitko pro nastaveni barvy bodu patrici do Juliovy mnoziny
    private JButton startColorJButton;                       // tlacitko pro nastaveni barvy bodu u nehoz se zjisti v posledni iteraci ze nepatri do mnoziny
    private JButton endColorJButton;                         // tlacitko pro nastaveni barvy bodu u nehoz se zjisti v 1. iteraci ze nepatri do mnoziny
    private JTextField realPartOfCJTextField;                // textove pole pro nastaveni realne casti parametru c, ktery specifikuje konkretni Juliovu mnozinu
    private JTextField imaginaryPartOfCJTextField;           // textove pole pro nastaveni imaginarni casti parametru c, ktery specifikuje konkretni Juliovu mnozinu
    private JTextField animationStepRealCJTextField;         // textove pole pro nastaveni hodnoty, ktera se pricte k realne casti parametru c v jednom animacnim kroku
    private JTextField animationStepImaginaryCJTextField;    // textove pole pro nastaveni hodnoty, ktera se pricte k imaginarni casti parametru c v jednom animacnim kroku
    private JTextField maxIterationJTextField;               // textove pole pro nastaveni max pocet iteraci pri zjistovani jestli dany bod patri nebo nepatric do juliovy mnoziny
    private JButton applyChangesJButton;                     // tlacitko kterym se aplikuji zmeny parametru
    private JuliasetVisualisation juliaSetVisualizationJPanel; // levy panel, ktery se stara o vizualiazaci juliovy mnoziny

    private Point mousePt; // kde bylo stisknuto tlačítko myši

    private JFrame mainJFrame; // hlavni obrazovka aplikace


    /**
     * V konstruktoru hlavniho panelu aplikace vytvorime GUI a nasatvime obsluzne rutiny udalosti
     * @param mainJFrame hlavni okno obrazovky
     */
    public App(JFrame mainJFrame) {

        // nejdrive si ulozime ke kteremi jframu patri tento jpanel
        this.mainJFrame = mainJFrame;

        // defaultní hodnoty parametrů
        Color juliaColorDefault = new Color(Integer.parseInt("-103"), true);
        Color startColorDefault = new Color(Integer.parseInt("-1"), true);
        Color endColorDefault = new Color(Integer.parseInt("-16764058"), true);
        int maxIterationsDefault = 100;
        double realPartOfCDefault = -0.7720000000000002;
        double imaginaryPartOfCDefault = -0.12100000000000041;
        double animationStepRealCDefault = 0.005;  // krok ktery pridavame/odebirame k realne casti parametru C pri stisknuti dane klavesy
        double animationStepImaginaryCDefault = 0.005; // krok ktery pridavame/odebirame k imaginarni casti parametru C pri stisknuti dane klavesy

        // ------ SESTAVIME GUI ------- //

        // hlavní panel
        mainJPanel = new JPanel();
        mainJPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 20, 20));
        //mainJPanel.setPreferredSize(new Dimension(WIDTH, HEIGHT));

        // levý panel kde se yvkresluje Juliova mnozina
        juliaSetVisualizationJPanel = new JuliasetVisualisation();
        // nastavime defaultni hodnoty parametru v levem vykreslovacim panelu
        juliaSetVisualizationJPanel.setC(realPartOfCDefault, imaginaryPartOfCDefault);
        juliaSetVisualizationJPanel.setAnimationStepRealC(animationStepRealCDefault);
        juliaSetVisualizationJPanel.setAnimationStepImaginaryC(animationStepImaginaryCDefault);
        juliaSetVisualizationJPanel.setJuliaColor(juliaColorDefault);
        juliaSetVisualizationJPanel.setStartColor(startColorDefault);
        juliaSetVisualizationJPanel.setEndColor(endColorDefault);
        juliaSetVisualizationJPanel.setMaxIterations(maxIterationsDefault);
        mainJPanel.add(juliaSetVisualizationJPanel);

        // pravý panel bude mit grid layout s 11 radky a 2 sloupci kde bude GUI ohledne nastaveni parametru
        rightJPanel = new JPanel();
        rightJPanel.setLayout(new GridLayout(11,2, 10, 10));
        //mainJPanel.setPreferredSize(new Dimension(WIDTH, HEIGHT));

        // 1. radek
        JLabel paramJLabel = new JLabel("Params:");
        Font tmpFont = paramJLabel.getFont();
        Font tmpNewFont = new Font(tmpFont.getFontName(), Font.BOLD, tmpFont.getSize() + 4);
        paramJLabel.setFont(tmpNewFont);
        rightJPanel.add(paramJLabel);
        rightJPanel.add(new JLabel("")); // invisible label to fill column in grid

        // 2. radek
        rightJPanel.add(new JLabel("Julia color:"));
        rightJPanel.add(new JLabel("")); // invisible label to fill column in grid

        // 3. radek
        juliaColorJButton = new JButton();
        juliaColorJButton.setOpaque(true);
        juliaColorJButton.setBackground(juliaColorDefault);
        rightJPanel.add(juliaColorJButton);
        rightJPanel.add(new JLabel("")); // invisible label to fill column in grid

        // 4 radek
        rightJPanel.add(new JLabel("Start color:"));
        rightJPanel.add(new JLabel("Stop color:"));

        // 5. radek
        startColorJButton = new JButton();
        startColorJButton.setOpaque(true);
        startColorJButton.setBackground(startColorDefault);
        rightJPanel.add(startColorJButton);
        endColorJButton = new JButton();
        endColorJButton.setOpaque(true);
        endColorJButton.setBackground(endColorDefault);
        rightJPanel.add(endColorJButton);

        // 6. radek
        rightJPanel.add(new JLabel("Real part of C:"));
        rightJPanel.add(new JLabel("Imaginary part of C:")); // invisible label to fill column in grid

        // 7. radek
        realPartOfCJTextField = new JTextField(10);
        realPartOfCJTextField.setText(String.valueOf(realPartOfCDefault));
        rightJPanel.add(realPartOfCJTextField);
        imaginaryPartOfCJTextField = new JTextField(10);
        imaginaryPartOfCJTextField.setText(String.valueOf(imaginaryPartOfCDefault));
        rightJPanel.add(imaginaryPartOfCJTextField);

        // 8. radek
        rightJPanel.add(new JLabel("Animation real C step:"));
        rightJPanel.add(new JLabel("Animation imaginary C step:"));

        // 9. radek
        animationStepRealCJTextField = new JTextField(10);
        animationStepRealCJTextField.setText(String.valueOf(animationStepRealCDefault));
        rightJPanel.add(animationStepRealCJTextField);
        animationStepImaginaryCJTextField = new JTextField(10);
        animationStepImaginaryCJTextField.setText(String.valueOf(animationStepImaginaryCDefault));
        rightJPanel.add(animationStepImaginaryCJTextField);

        // 10. radek
        rightJPanel.add( new JLabel("Max. iterations:"));
        rightJPanel.add(new JLabel("")); // invisible label to fill column in grid

        // 11. radek
        maxIterationJTextField = new JTextField(10);
        maxIterationJTextField.setText(String.valueOf(maxIterationsDefault));
        rightJPanel.add(maxIterationJTextField);
        applyChangesJButton = new JButton("Apply changes");
        rightJPanel.add(applyChangesJButton);

        mainJPanel.add(Box.createHorizontalStrut(0));
        mainJPanel.add(rightJPanel);
        // automaticky nastavime velikost okna aplikace podle obsahu
        mainJPanel.setPreferredSize(new Dimension(mainJPanel.getPreferredSize().width + 20, mainJPanel.getPreferredSize().height));
        mainJPanel.setFocusable( true );

        createMenu();       // vytvorime menu
        addEventHandlers(); // nastavime obsluzne rutiny udalosti
    }

    /**
     * Funkce vytvori v GUI menu
     */
    public void createMenu() {
        mainJMenuBar = new JMenuBar();
        JMenu fileJMenu = new JMenu("File");

        // otevreni existujici vizualizace
        openJMenuItem = new JMenuItem("Open visualization");
        // ulozeni soucasne vizualizace
        saveJMenuItem = new JMenuItem("Save visualization");

        // do file menu ulozime polozky open a save
        fileJMenu.add(openJMenuItem);
        fileJMenu.add(saveJMenuItem);

        // file menu ulozime do hlavniho menu
        mainJMenuBar.add(fileJMenu);

        // -----------------------------------------
        // edit menu
        JMenu editJMenu = new JMenu("Edit");
        // resetovani pohledu
        resetViewJMenuItem = new JMenuItem("Reset view");

        // do edit menu ulozime polozku reset viev
        editJMenu.add(resetViewJMenuItem);

        // edit menu ulozime do hlavniho menu
        mainJMenuBar.add(editJMenu);

        // -----------------------------------------
        // info menu
        JMenu infoJMenu = new JMenu("Info");
        // napoveda
        helpJMenuItem = new JMenuItem("Help");
        // doinfoJMenu ulozime polozku help
        infoJMenu.add(helpJMenuItem);

        // o autorovi
        aboutJMenuItem = new JMenuItem("About program");
        // doinfoJMenu ulozime polozku about program
        infoJMenu.add(aboutJMenuItem);

        // edit menu ulozime do hlavniho menu
        mainJMenuBar.add(infoJMenu);
    }

    /**
     * Nastaveni obsluznych rutin udalosti
     */
    public void addEventHandlers() {

        /**
         * Pri kliknuti kdekoliv v hlavnim panelu ziska tento focus, aby jsme mohli pouzivat klavesy
         */
        mainJPanel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                mainJPanel.requestFocusInWindow();
            }
        });

        /**
         * Animujeme odectenim z realne casti C
         */
        mainJPanel.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, 0), "performAnimationStepSubstractFromRealC");
        mainJPanel.getActionMap().put("performAnimationStepSubstractFromRealC", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                performAnimationStep(KeyEvent.VK_LEFT);
            }
        });

        /**
         * Animujeme pridanim k realne casti C
         */
        mainJPanel.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, 0), "performAnimationStepAddToRealC");
        mainJPanel.getActionMap().put("performAnimationStepAddToRealC", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                performAnimationStep(KeyEvent.VK_RIGHT);
            }
        });

        /**
         * Animujeme odectenim z imaginarni casti C
         */
        mainJPanel.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, 0), "performAnimationStepSubstractFromImaginaryC");
        mainJPanel.getActionMap().put("performAnimationStepSubstractFromImaginaryC", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                performAnimationStep(KeyEvent.VK_DOWN);
            }
        });

        /**
         * Animujeme pridanim k imaginarni casti C
         */
        mainJPanel.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_UP, 0), "performAnimationStepAddToImaginaryC");
        mainJPanel.getActionMap().put("performAnimationStepAddToImaginaryC", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                performAnimationStep(KeyEvent.VK_UP);
            }
        });

        /**
         * Zvětšení/Zmenšení vybrane oblasti komplexni roviny pomoci kolecka mysi
         */
        juliaSetVisualizationJPanel.addMouseWheelListener(new MouseWheelListener() {
            @Override
            public void mouseWheelMoved(MouseWheelEvent e) {
                int direction = e.getWheelRotation();
                // WHEEL UP
                if (direction < 0) {
                    juliaSetVisualizationJPanel.changeZoom(e.getScrollAmount() * (-1));
                }
                // WHEEL DOWN
                else {
                    juliaSetVisualizationJPanel.changeZoom(e.getScrollAmount() * (1));
                }
                juliaSetVisualizationJPanel.repaint();
            }
        });

        /**
         * Posun ve vykreslované oblasti pomoci tahnuti mysi
         */
        juliaSetVisualizationJPanel.addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseDragged(MouseEvent e) {
                super.mouseDragged(e);
                int dx = e.getX() - mousePt.x;
                int dy = e.getY() - mousePt.y;

                juliaSetVisualizationJPanel.changeCenter(dx, dy);
                juliaSetVisualizationJPanel.repaint();
            }
        });

        /**
         * Zmena kurzoru mysi pokud je nad vykreslovanou oblasti abychm naznacili ze se s touto oblasti da pomoci mysi manipulovat
         * Take pri kliknuti ulozeni kurzoru mysi coz se pouzije pri udalosti on mouse dragged
         */
        juliaSetVisualizationJPanel.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                mousePt = e.getPoint();
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                super.mouseEntered(e);
                mainJPanel.setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
            }

            @Override
            public void mouseExited(MouseEvent e) {
                super.mouseEntered(e);
                mainJPanel.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
            }
        });

        /**
         * Zmena barvy Julia color pri kliknuti na dane tlacitko
         */
        juliaColorJButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if(e.getSource() == juliaColorJButton) {
                    Color newColor = JColorChooser.showDialog(null, "Choose a color", Color.RED);
                    juliaColorJButton.setBackground(newColor);
                }
            }
        });
        /**
         * Zmena barvy Start color pri kliknuti na dane tlacitko
         */
        startColorJButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if(e.getSource() == startColorJButton) {
                    Color newColor = JColorChooser.showDialog(null, "Choose a color", Color.RED);
                    startColorJButton.setBackground(newColor);
                }
            }
        });
        /**
         * Zmena barvy End color pri kliknuti na dane tlacitko
         */
        endColorJButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if(e.getSource() == endColorJButton) {
                    Color newColor = JColorChooser.showDialog(null, "Choose a color", Color.RED);
                    endColorJButton.setBackground(newColor);
                }
            }
        });

        /**
         * Pri potvrzeni zmeny prametru podle nich prekreslime vizualizaci
         */
        applyChangesJButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if(e.getSource() == applyChangesJButton) {
                    // nejdrive nastavime parametr kompelxni cislo C
                    double realPartOfC = 0.0;
                    double imaginaryPartOfC = 0.0;
                    int maxIterations = 0;
                    double animationStepRealC = 0.0;
                    double animationStepImaginaryC = 0.0;
                    // parsujeme parametry
                    try {
                        realPartOfC = Double.parseDouble(realPartOfCJTextField.getText());
                    }
                    catch (NumberFormatException exception) {
                        JOptionPane.showMessageDialog(mainJPanel, "Param 'Real part of C' cannot be converted to double datatype", "NumberFormatException", JOptionPane.ERROR_MESSAGE);
                    }
                    try {
                        imaginaryPartOfC = Double.parseDouble(imaginaryPartOfCJTextField.getText());
                    }
                    catch (NumberFormatException exception) {
                        JOptionPane.showMessageDialog(mainJPanel, "Param 'Imaginary part of C' cannot be converted to double datatype", "NumberFormatException", JOptionPane.ERROR_MESSAGE);
                    }
                    try {
                        animationStepRealC = Double.parseDouble(animationStepRealCJTextField.getText());
                    } catch (NumberFormatException exception) {
                        JOptionPane.showMessageDialog(mainJPanel, "Param 'Animation real C step' cannot be converted to double datatype", "NumberFormatException", JOptionPane.ERROR_MESSAGE);
                    }
                    try {
                        animationStepImaginaryC = Double.parseDouble(animationStepImaginaryCJTextField.getText());
                    } catch (NumberFormatException exception) {
                        JOptionPane.showMessageDialog(mainJPanel, "Param 'Animation imaginary C step' cannot be converted to double datatype", "NumberFormatException", JOptionPane.ERROR_MESSAGE);
                    }
                    try {
                        maxIterations = Integer.parseInt(maxIterationJTextField.getText());
                    }
                    catch (NumberFormatException exception) {
                        JOptionPane.showMessageDialog(mainJPanel, "Param 'Max iterations' cannot be converted to int datatype", "NumberFormatException", JOptionPane.ERROR_MESSAGE);
                    }

                    // parsovane parametry nastavime v panelu starajici se o vykresleni vizualizace
                    juliaSetVisualizationJPanel.setC(realPartOfC, imaginaryPartOfC);
                    juliaSetVisualizationJPanel.setAnimationStepRealC(animationStepRealC);
                    juliaSetVisualizationJPanel.setAnimationStepImaginaryC(animationStepImaginaryC);

                    juliaSetVisualizationJPanel.setJuliaColor(juliaColorJButton.getBackground());
                    juliaSetVisualizationJPanel.setStartColor(startColorJButton.getBackground());
                    juliaSetVisualizationJPanel.setEndColor(endColorJButton.getBackground());

                    juliaSetVisualizationJPanel.setMaxIterations(maxIterations);
                    juliaSetVisualizationJPanel.repaint();
                }
            }
        });

        /**
         * Open existing visualization
         */
        openJMenuItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if(e.getSource() == openJMenuItem) {

                    // nechame uzivatele aby si zvolil existujici viualizaci ve formatu png, ze ktere nacteme z metadat parametry vizualizace a tuto vykreslime
                    try {
                        JFileChooser fileChooser = new JFileChooser();
                        fileChooser.setFileFilter(new FileNameExtensionFilter("PNG files", "png"));
                        fileChooser.setCurrentDirectory(new File(System.getProperty("user.dir") + "/images"));
                        if (fileChooser.showOpenDialog(mainJPanel) == JFileChooser.APPROVE_OPTION) {
                            File fileToOpen = fileChooser.getSelectedFile();
                            juliaSetVisualizationJPanel.openVisualizationFromFile(fileToOpen);

                            // ziskame nactene parametry
                            Color juliaColorDefault = juliaSetVisualizationJPanel.getJuliaColor();
                            Color startColorDefault = juliaSetVisualizationJPanel.getStartColor();
                            Color endColorDefault = juliaSetVisualizationJPanel.getEndColor();
                            int maxIterationsDefault = juliaSetVisualizationJPanel.getMaxIterations();
                            double realPartOfC = juliaSetVisualizationJPanel.getC().getRealPart();
                            double imaginaryPartOfC = juliaSetVisualizationJPanel.getC().getImaginaryPart();
                            double animationStepRealC = juliaSetVisualizationJPanel.getAnimationStepRealC();
                            double animationStepImaginaryC = juliaSetVisualizationJPanel.getAnimationStepImaginaryC();

                            // a nactene parametry ulozime do gui
                            juliaColorJButton.setBackground(juliaColorDefault);
                            startColorJButton.setBackground(startColorDefault);
                            endColorJButton.setBackground(endColorDefault);
                            maxIterationJTextField.setText(String.valueOf(maxIterationsDefault));
                            realPartOfCJTextField.setText(String.valueOf(realPartOfC));
                            imaginaryPartOfCJTextField.setText(String.valueOf(imaginaryPartOfC));
                            animationStepRealCJTextField.setText(String.valueOf(animationStepRealC));
                            animationStepImaginaryCJTextField.setText(String.valueOf(animationStepImaginaryC));


                            // nakonec repaintneme vizualizaci podle novych parametru
                            juliaSetVisualizationJPanel.repaint();

                        }
                    } catch (Exception exception) {
                        JOptionPane.showMessageDialog(mainJPanel, "Error during opening file", "Exception", JOptionPane.ERROR_MESSAGE);
                    }
                }
            }
        });

        /**
         * Save current visualization
         */
        saveJMenuItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if(e.getSource() == saveJMenuItem) {
                    // nechame uzivatele aby si zvolil rozliseni ukladane vizualizace a tuto ulozime
                    try {
                        int outputResolutionWidthInt = juliaSetVisualizationJPanel.IMAGE_WIDTH; // výstupní šířka obrázku kterou si můe uživatel nastavit
                        int outputResolutionHeightInt = juliaSetVisualizationJPanel.IMAGE_HEIGHT; // výstupní výška obrázku, kterou si může uživatel nastavit
                        // nejdřív s uživatel vybere výstupní rozlišení obrázku
                        String outputResolution = JOptionPane.showInputDialog(mainJPanel, "Please enter output image resolution", juliaSetVisualizationJPanel.IMAGE_WIDTH + "x" + juliaSetVisualizationJPanel.IMAGE_HEIGHT);
                        // uživaelem zadané rozlišení se pokusíme zparsovat
                        try {
                            String[] resolutionParts = outputResolution.split("x");
                            String outputResolutionWidth = resolutionParts[0];
                            String outputResolutionHeight = resolutionParts[1];
                            outputResolutionWidthInt = Integer.parseInt(outputResolutionWidth);
                            outputResolutionHeightInt = Integer.parseInt(outputResolutionHeight);
                        }
                        catch (Exception ee){
                            JOptionPane.showMessageDialog(mainJPanel, "Specified resolution is incorrect", "Exception", JOptionPane.ERROR_MESSAGE);
                        }

                        // nechame uzivatele aby si zvolil vystupni soubor do ktereho se ulozi vizualizace i s metadaty popisujici parametry vizualizace
                        JFileChooser fileChooser = new JFileChooser();
                        fileChooser.setSelectedFile(new File("my_visualization.png"));
                        fileChooser.setFileFilter(new FileNameExtensionFilter("PNG files","png"));
                        fileChooser.setCurrentDirectory(new File( System.getProperty("user.dir") + "/images"));
                        if (fileChooser.showSaveDialog(mainJPanel) == JFileChooser.APPROVE_OPTION) {
                            String tmpFilePathName = System.getProperty("user.dir") + "\\out.png";
                            File fileToSave = fileChooser.getSelectedFile();
                            File tmpFile = new File( tmpFilePathName);
                            juliaSetVisualizationJPanel.createRasterImage(outputResolutionWidthInt, outputResolutionHeightInt); // těsně před uložením obrázku tento obrázek v požadovaném rozlišení vygnerujeme
                            ImageIO.write(juliaSetVisualizationJPanel.getImage(), "png",tmpFile);
                            juliaSetVisualizationJPanel.saveVisualizationToFile(tmpFile, fileToSave);
                            tmpFile.delete();
                        }
                    }
                    catch(IOException exception) {
                        JOptionPane.showMessageDialog(mainJPanel, "Error during saving file", "IOException", JOptionPane.ERROR_MESSAGE);
                        exception.printStackTrace(System.err);
                    }
                    catch(Exception exception) {
                        JOptionPane.showMessageDialog(mainJPanel, "Error during saving file", "Exception", JOptionPane.ERROR_MESSAGE);
                        exception.printStackTrace(System.err);
                    }
                }
            }
        });

        /**
         * Pri kliknuti na danou polozku menu resetneme vyber oblasti komplexni roviny
         */
        resetViewJMenuItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if(e.getSource() == resetViewJMenuItem) {
                    juliaSetVisualizationJPanel.resetView();
                    juliaSetVisualizationJPanel.repaint();
                }
            }
        });

        /**
         * Pri kliknuti na danou polozku menu zobrazime napovedu programu
         */
        helpJMenuItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if(e.getSource() == helpJMenuItem) {
                    JOptionPane.showMessageDialog(mainJPanel, "Tips to using program:\n\n1) On left side is visualized Julia set in complex plane. On right side you can change visualization params.\n\n2) Absolute value of C should be relatively small.\n\n3) Max. iteration should not be large number, because then is visualization slow.\n\n4) Animation steps are used when you press arrow keys (up, down, left, right) on keyboard, then visualization is animated by changing param C. \n\n5) You can use mouse dragging to move on complex plane and mouse scrolling to zoom in and zoom out complex plane.\n\n6) Julia color has points which are in Julia set, Start color has points which are almost in Julia set (we know they aren't in last iteration), End color has points which are certainly not in Julia set (we know already in 1. iteration)", "Tips to using program", JOptionPane.INFORMATION_MESSAGE);
                }
            }
        });

        /**
         * Pri kliknuti na danou polozku menu zobrazime info o programu
         */
        aboutJMenuItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if(e.getSource() == aboutJMenuItem) {
                    JOptionPane.showMessageDialog(mainJPanel, "About program:\n\n1) This program/application was created in year 2017 within project in cource SFC (Soft computing) on 'Faculty of Information Technology, Brno University of Technology'. \n\n2) Purpouse of this program is visualization of Julia set.\n\n3) Author of this program is Jan Herec, herech@seznam.cz.", "About program", JOptionPane.INFORMATION_MESSAGE);
                }
            }
        });

    }

    /**
     * Funkce provede animacni krok skrze upravu parametru C na zaklade stisku sipky
     */
    public void performAnimationStep(int keyPressed) {
        double realPartOfC = 0.0;
        double imaginaryPartOfC = 0.0;

        double stepRealC = 0.0;
        double stepImaginaryC = 0.0;

        // nejdrive zjistim aktualni hodnoty parametru C
        try {
            realPartOfC = Double.parseDouble(realPartOfCJTextField.getText());
        } catch (NumberFormatException exception) {
            JOptionPane.showMessageDialog(mainJPanel, "Param 'Real part of C' cannot be converted to double datatype", "NumberFormatException", JOptionPane.ERROR_MESSAGE);
        }
        try {
            imaginaryPartOfC = Double.parseDouble(imaginaryPartOfCJTextField.getText());
        } catch (NumberFormatException exception) {
            JOptionPane.showMessageDialog(mainJPanel, "Param 'Imaginary part of C' cannot be converted to double datatype", "NumberFormatException", JOptionPane.ERROR_MESSAGE);
        }
        try {
            stepRealC = Double.parseDouble(animationStepRealCJTextField.getText());
        } catch (NumberFormatException exception) {
            JOptionPane.showMessageDialog(mainJPanel, "Param 'Animation real C step' cannot be converted to double datatype", "NumberFormatException", JOptionPane.ERROR_MESSAGE);
        }
        try {
            stepImaginaryC = Double.parseDouble(animationStepImaginaryCJTextField.getText());
        } catch (NumberFormatException exception) {
            JOptionPane.showMessageDialog(mainJPanel, "Param 'Animation imaginary C step' cannot be converted to double datatype", "NumberFormatException", JOptionPane.ERROR_MESSAGE);
        }

        // na zaklde zmaknute kalvesu provedu zmenu parametru C
        switch(keyPressed) {
            // sipka nahoru pridava hodnotu ke komplexni casti parametru C
            case KeyEvent.VK_UP:
                imaginaryPartOfC += stepImaginaryC;
                break;
            // sipka dolu odebira hodnotu komplexni casti parametru C
            case KeyEvent.VK_DOWN:
                imaginaryPartOfC -= stepImaginaryC;
                break;
            // sipka vlevo odebira hodnotu realne casti parametru C
            case KeyEvent.VK_LEFT:
                realPartOfC -= stepRealC;
                break;
            // sipka vpravo pridava hodnotu realne casti parametru C
            case KeyEvent.VK_RIGHT :
                realPartOfC += stepRealC;
                break;
        }

        // na zaklde zmeneneho parametru C provedeme prekresleni
        realPartOfCJTextField.setText(String.valueOf(realPartOfC));
        imaginaryPartOfCJTextField.setText(String.valueOf(imaginaryPartOfC));
        juliaSetVisualizationJPanel.setC(realPartOfC, imaginaryPartOfC);
        juliaSetVisualizationJPanel.repaint();

    }

    /**
     * Vstupni bod programu, ve kterem vytvorime aplikaci
     */
    public static void main(String[] args) {
        UIManager.put("Button.font", new FontUIResource("Arial", Font.BOLD, 12)); // nastavime globalne vychozi font
        JFrame mainJFrame = new JFrame("Julia set visualization");
        App app = new App(mainJFrame);
        mainJFrame.setContentPane(app.mainJPanel);
        mainJFrame.setJMenuBar(app.mainJMenuBar);
        mainJFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainJFrame.setResizable(false);
        mainJFrame.setFocusable(true);
        mainJFrame.pack();
        mainJFrame.setLocationRelativeTo(null);
        mainJFrame.setVisible(true);

    }
}
